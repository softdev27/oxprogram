/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class MainProgram {
    public static void main(String[] args) {
        OXGameSystem t = new OXGameSystem();
        boolean gameStatus = false;          
        t.showTitle();
        do {
          
            t.updateTableMap();
            if (t.checkForWinner()) {
                t.printTableMap();
                System.out.println(">>> " + t.winnerPlayer()  + " win <<<");
                gameStatus = true;
            }
            if (t.isTableMapFull()) {
                t.printTableMap();
                System.out.println(">>>draw<<<");
                gameStatus = true;
            }
            t.changePlayer();  
        } while (gameStatus == false); 
        
        
    }
}
