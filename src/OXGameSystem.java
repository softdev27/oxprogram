
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class OXGameSystem {
    private char[][] TableMap;               
    private char Player;           
    private int rows = 3;
    private int columns = 3;
    
    public static Scanner sc = new Scanner(System.in);
    
    public OXGameSystem() {
        TableMap = new char[rows][columns];           
        Player = 'o';              
        initializeTableMap();
    }
    
    public void initializeTableMap() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                TableMap[i][j] = '-';
            }
        }
    }
    public void printTableMap() {
        for(int i = 0; i < rows; i++) {
            System.out.print("");
            for(int j = 0; j < columns; j++) {
                System.out.print(TableMap[i][j]+" " );
            }
            System.out.println("");
       
        }
    }
    
    public boolean isTableMapFull() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                if (TableMap[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
    public boolean checkForWinner() {
        return (checkRows() || checkColumns() || checkDiagonals());
    }
    
    private boolean checkRows() {
        for (int i = 0; i < rows; i++) {
            if (TableMap[i][0] != '-' && TableMap[i][0] == TableMap[i][1] && 
                TableMap[i][0] == TableMap[i][2]) {
                return true;
            }
        }
        return false;
    }
    
    private boolean checkColumns() {
        for (int i = 0; i < columns; i++) {
            if (TableMap[0][i] != '-' && TableMap[0][i] == TableMap[1][i] && 
                TableMap[0][i] == TableMap[2][i]) {
                return true;
            }
        }
        return false;
    }
    
    private boolean checkDiagonals() {
        return (TableMap[0][0] != '-' && TableMap[0][0] == TableMap[1][1] 
                && TableMap[0][0] == TableMap[2][2]) 
                ||
               (TableMap[0][2] != '-' && TableMap[0][2] == TableMap[1][1] 
                && TableMap[0][2] == TableMap[2][0]);
                
    }
    
    public char changePlayer() {
        return Player = (Player == 'o') ? 'x' : 'o';
    }
    
    public void updateTableMap() {
        boolean validInput = false;    
         printTableMap();
        do {
           
         if (Player == 'o') {
            System.out.println("Turn o");
             System.out.println("Please input your row, col : ");
         } else {
            System.out.println("Turn x");
            System.out.println("Please input your row, col : ");
         }
         int row = sc.nextInt() - 1; 
         int col = sc.nextInt() - 1;
         
         if (row >= 0 && row < rows && col >= 0 && col < columns 
             && TableMap[row][col] == '-') {
            TableMap[row][col] = Player; 
            validInput = true; 
         } else {
            System.out.println("Sorry, the move at (" 
                               + (row + 1) + ", " + (col + 1)
                               + ") is not valid. Please try again");
         }
      } while (!validInput); 
    }
     public void showTitle() {
        System.out.println("Welcom to OX game");
    }
public char winnerPlayer(){
   return  changePlayer();
}
}
